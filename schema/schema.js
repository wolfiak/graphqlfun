const graphql = require("graphql");
const fetch = require("node-fetch");
const util = require("util");
const parseXML = util.promisify(require("xml2js").parseString);
const _ = require("lodash");

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLInt,
  GraphQLList,
  GraphQLID
} = graphql;

const enviroment = {
  key: "W9cOp7fdr9jGV1sr9Hl1Q"
};

const BookType = new GraphQLObjectType({
  name: "Book",
  fields: () => ({
    title: {
      type: GraphQLString,
      resolve: book => {
        return book.title[0];
      }
    },
    isbn: {
      type: GraphQLString,
      resolve: book => {
        return book.isbn[0];
      }
    }
  })
});

const AuthorType = new GraphQLObjectType({
  name: "Author",
  fields: () => ({
    name: {
      type: GraphQLString,
      resolve: xml => {
        return xml.GoodreadsResponse.author[0].name[0];
      }
    },
    books: {
      type: GraphQLList(BookType),
      resolve: xml => {
        return xml.GoodreadsResponse.author[0].books[0].book;
      }
    }
  })
});

module.exports = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: "Query",
    fields: () => ({
      author: {
        type: AuthorType,
        args: {
          id: { type: GraphQLID }
        },
        resolve: (root, args) => {
          return fetch(
            `https://www.goodreads.com/author/show.xml?id=${args.id}&key=${
              enviroment.key
            }`
          )
            .then(res => res.text())
            .then(parseXML);
        }
      }
    })
  })
});
